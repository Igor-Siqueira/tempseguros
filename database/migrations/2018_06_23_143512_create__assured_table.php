<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssuredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Assured', function (Blueprint $table) {
            $table->increments('idAssured');
            $table->integer('userid')->unsigned();
            $table->foreign('userid')->references('id')->on('users')->onDelete('cascade');
            $table->string('Name', 300);
            $table->string('Cpf', 50);
            $table->string('Cep', 50);
            $table->string('State', 300);
            $table->string('City', 300);
            $table->string('Street', 300);
            $table->string('Neighborhood', 300);
            $table->string('Number', 9);
            $table->string('Complement', 300);
            $table->string('PhoneFix', 50);
            $table->string('PhoneCel', 50);
            $table->string('Printname', 900);
            $table->string('Creditnumber', 900);
            $table->string('Securecode', 900);
            $table->string('Datevalidate',900);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Assured');
    }
}

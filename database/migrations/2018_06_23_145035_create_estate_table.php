<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Estate', function (Blueprint $table) {
            $table->integer('iduser')->unsigned();;
            $table->foreign('iduser')->references('id')->on('users');
            $table->increments('idEstate'); 
            $table->string('Name', 50);
            $table->string('UrlPhoto', 500);
            $table->integer('idCategoryEstate')->unsigned();
            $table->foreign('idCategoryEstate')->references('idCategoryEstate')->on('CategoryEstate');
            $table->string('Modelo', 100);
            $table->string('Description', 200);
            $table->string('Placa', 8)->nullable();
            $table->string('Renavam', 11)->nullable();
            $table->string('Chassi', 17)->nullable();
            $table->string('Color', 30)->nullable();
            $table->string('Year', 4)->nullable();
            $table->string('Serial', 12)->nullable();
            $table->string('Imei', 15)->nullable();
            $table->string('State', 200)->nullable();
            $table->string('City', 200)->nullable();
            $table->string('Street', 200)->nullable();
            $table->string('Neighborhood', 200)->nullable();
            $table->string('Number', 9)->nullable();
            $table->string('Cep', 20)->nullable();
            $table->string('Complement', 200)->nullable();
            $table->string('Dimensions', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estate');
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'assured'], function() {
    Route::get('/', 'AssuredController@create');
    Route::get('/edit/{id}', 'AssuredController@edit');
    Route::post('/save', 'AssuredController@save');
});

Route::group(['prefix' => 'insurance'], function() {
    Route::get('/', 'InsurancePolicyController@index');
    Route::get('/create', 'InsurancePolicyController@create');
    Route::get('/save', 'InsurancePolicyController@index');
    Route::get('/edit/{id}', 'InsurancePolicyController@index');
    
});

Route::group(['prefix' => 'estate'], function() {
    Route::get('/', 'EstateController@index');
    Route::get('/create', 'EstateController@create');
    Route::get('/edit/{id}', 'EstateController@edit');
    Route::post('/save', 'EstateController@save');
});
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
 
  <link href="{{ asset('landpage/img/favicon.png') }}" rel="icon">
  <link href="{{asset('landpage/landpage/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
 
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
 
  <link href="{{asset ('landpage/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  
  <link href="{{asset('landpage/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('landpage/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('landpage/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('landpage/lib/magnific-popup/magnific-popup.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('landpage/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Avilon
    Theme URL: https://bootstrapmade.com/avilon-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        
        <h1><a href="{{ url('/') }}" class="scrollto">
          <a href="#intro"><img src="landpage/img/logo2.png" alt="top" title="">
          TEMPSEGUROS</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Inicio</a></li>
          <li><a href="#about">Sobre Nós</a></li>
          <li><a href="#features">Funcionalidade</a></li>
          <li><a href="#team">Time</a></li>
         <li><a href="{{ route('login') }}">Login</a></li>
          <li><a href="#contact">Contato</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

    <div class="intro-text">
      <h2><u>TempSeguros</u></h2>
      <p>A MELHOR SOLUÇÃO DE SEGUROS SOB DEMANDA</p>
      <a href="{{ route('register') }}" class="btn-get-started scrollto">Começe agora</a>
    </div>

    <div class="product-screens">

      <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
        <img src="landpage/img/product-screen-1.png" alt="">
      </div>

      <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
        <img src="landpage/img/product-screen-2.png" alt="">
      </div>

      <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
        <img src="landpage/img/product-screen-3.png" alt="">
      </div>

    </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">Seguro de acordo com sua necessidade, <b>PAGANDO SOMENTE O QUE USAR !</b></h3>
          <span class="section-divider"></span>
          <p class="section-description">
            
          </p>
        </div>

        <div class="row">
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="landpage/img/about-img.jpg" alt="">
          </div>

          <div class="col-lg-6 content wow fadeInRight"
            <h2></h2>
            <h3> O PODER DE ESCOLHA ESTÁ EM SUAS MÃOS ! </h3>
            <p>
            Pensando 100% em você cliente, criamos o conceito “seguro das coisas” que pode ser contratado a qualquer momento e de qualquer lugar, e o melhor, você pode utilziar apenas pelo período e serviço que você escolher.
            </p>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i> <b>Tenha a liberdade de escolha.</b></li>
              <p>Não fique preso com tempo de fidelização, faça o contrato por dias, semanas e porque não por horas.</p>
              <li><i class="ion-android-checkmark-circle"></i> <b>Tudo no mesmo lugar.</b></li>
              <p>Seguro de diferentes bens na mesma plataforma.</p>
            </ul>

          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Product Featuress Section
    ============================-->
    <section id="features">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 offset-lg-4">
            <div class="section-header wow fadeIn" data-wow-duration="1s">
              <h3 class="section-title">Funcionalidades</h3>
              <span class="section-divider"></span>
            </div>
          </div>

          <div class="col-lg-4 col-md-5 features-img">
            <img src="landpage/img/product-features.png" alt="" class="wow fadeInLeft">
          </div>

          <div class="col-lg-8 col-md-7 ">

            <div class="row">

              <div class="col-lg-6 col-md-6 box wow fadeInRight">
                <div class="icon"><i class="ion-card"></i></div>
                <h4 class="title"><a href="">Pagamento Sob Demanda</a></h4>
                <p class="description">Pague somente pelo que utilizar. Adquirá um seguro quando e por quanto tempo precisar.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.1s">
                <div class="icon"><i class="ion-lock-combination"></i></div>
                <h4 class="title"><a href="">Seguro das Coisas</a></h4>
                <p class="description">Faça o seguro de todos os seus bens materiais. Ative ou inative de forma personalizada seu seguro.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight data-wow-delay="0.2s">
                <div class="icon"><i class="ion-social-buffer-outline"></i></div>
                <h4 class="title"><a href="">Gerenciamento</a></h4>
                <p class="description">Gerencie de forma ágil e inteligente, diretamente do seu smartphone.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.3s">
                <div class="icon"><i class="ion-ios-stopwatch-outline"></i></div>
                <h4 class="title"><a href="">Agendar Ativação</a></h4>
                <p class="description">Agende a ativação e finalização do seu seguro de acordo com sua necessidade.</p>
              </div>
            </div>

          </div>

        </div>

      </div>

    </section><!-- #features -->

    <!--==========================
      Product Advanced Featuress Section
    ============================-->
    <section id="advanced-features">

      <div class="features-row section-bg">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-right wow fadeInRight" src="landpage/img/seguro.png" alt="">
              <div class="wow fadeInLeft">
                <h2>Mas afinal, o que é " Seguro das Coisas " ? </h2>
                <h3>Entenda sobre o mais novo conceito de seguros.</h3>
                <p>Atualmente a necessidade de assegurar bens vem crescendo exponencialmente, porém certos produtos necessitam desse serivço.</p>
                 <p>A partir desse conceito elaboramos um novo modelo de serviço, o <strong>seguro das coisas</strong>, que tem como objetivo assegurar todos os seus bens, de forma centralizada e dinâmica.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="features-row">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-left" src="landpage/img/advanced-feature-2.jpg" alt="">
              <div class="wow fadeInRight">
                <h2> O que eu posso assegurar ?</h2>
                <h3>Saiba mais sobre os serviços oferecidos.</h3>
                <i class="ion-android-car" class="wow fadeInRight" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-duration="0.5s"><b>Seguro Auto </b> - Seu carro seguro pra você curtir a vida. Você escolhe as coberturas e deixa tudo do seu jeito.</p>
                <i class="ion-iphone" data-wow-delay="0.2s" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.5s"><b>Seguro Smart</b> - Com o seguro para smartphone você não corre o risco de ficar sem o aparelho se algum imprevisto acontecer. </p>
                <i class="ion-home" data-wow-delay="0.4" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-delay="0.4s" data-wow-duration="0.5s"><b>Seguro Home</b> - Um seguro residencial para aproveitar sua casa ao máximo.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    <!--==========================
      <div class="features-row section-bg">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-right wow fadeInRight" src="landpage/img/advanced-feature-3.jpg" alt="">
              <div class="wow fadeInLeft">
                <h2>Duis aute irure dolor in reprehenderit in voluptate velit esse</h2>
                <h3>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                <i class="ion-ios-albums-outline"></i>
                <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    ==========================-->

    </section><!-- #advanced-features -->

    <!--==========================
      Banner entre para o Clube
    ============================-->
    <section id="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Seja Já um Segurado ! </h3>
            <p class="cta-text"> Gostou do que viu ? Está interessado em nosso produto ? Faça já o seu cadastro e começe agora mesmo a desfrutar de nossos serviços. É Bem fácil, eu garanto.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="{{ route('register') }}">Entre para o Clube</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      More Features Section
    ============================-->
    <section id="more-features" class="section-bg">
      <div class="container">

        <div class="section-header">
          <h3 class="section-title">Mais Funcionalidades</h3>
          <span class="section-divider"></span>
          <p class="section-description">Tenha mais facilidade para gerenciar seu seguro, de acordo com sua demanda.</p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="ion-clipboard"></i></div>
              <h4 class="title"><a href="#">Contratação</a></h4>
              <p class="description">Contrate quando e onde você precisar.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="ion-grid"></i></div>
              <h4 class="title"><a href="">Dashboard de Controle</a></h4>
              <p class="description">Visualize e compreenda facilmente seu gastos.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="ion-stats-bars"></i></div>
              <h4 class="title"><a href="">Relatório</a></h4>
              <p class="description">Visualize os relatórios de suas ativações e gastos quando quiser.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="ion-help-buoy"></i></div>
              <h4 class="title"><a href="">Assistência 24 horas</a></h4>
              <p class="description">Tenha assistência 24 horas para solucionar seus problemas e ocorrências.</p>
            </div>
          </div>

        </div>
      </div>
    </section><!-- #more-features -->

   

    <!--==========================
      Frequently Asked Questions Section
    ============================-->
    <section id="faq">
      <div class="container">

        <div class="section-header">
          <h3 class="section-title">Perguntas Frequentes</h3>
          <span class="section-divider"></span>
          <p class="section-description">Compreenda como funciona o seguro sob demanda, e tenha acesso a essa facilidade agora.</p>
        </div>

        <ul id="faq-list" class="wow fadeInUp">
          <li>
            <a data-toggle="collapse" class="collapsed" href="#faq1">Como efetuo o cadastro ?<i class="ion-android-remove"></i></a>
            <div id="faq1" class="collapse" data-parent="#faq-list">
              <p>
                Ao acessar a página inicial do nosso site, clique no botão “Comece agora”. Você será redirecionado para uma tela de cadastro, onde deverá preencher com seus dados e criar uma senha.</p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Como asseguro o meu bem ?<i class="ion-android-remove"></i></a>
            <div id="faq2" class="collapse" data-parent="#faq-list">
              <p>
                Após efetuar o login, acesse a opção de “Ativar Seguro”, clique na opção “Criar Novo Seguro”, se o bem já estiver cadastrado siga a partir do <b> passo 2.</b> </p>

                <p><b> Passo 1 </b> -  Após efetuar o login, acesse a opção de “Bens”, você será redirecionado a uma página onde efetuará o cadastro de seu bem. Após o preenchimento ter sido concluído você deve clicar em salvar. </p>

                <p><b> Passo 2 </b> – Selecione o bem a ser assegurado, clique em “avançar”. Você será redirecionado a uma página, onde você deve selecionar as características e benefícios que deseja contratar. O valor irá alterar de acordo com suas escolhas e período que você deseja assegurar. Após a seleção, clique em “Efetuar Pagamento”. 
                Você será redirecionado a uma página onde você deverá escolher a forma de pagamento, após preencher todos os campos corretamente, você deve clicar em “Concluir”, desta forma o pagamento será concluído e o seguro será ativado.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Como informo um sinistro ? <i class="ion-android-remove"></i></a>
            <div id="faq3" class="collapse" data-parent="#faq-list">
              <p>
               Após efetuar o login, o menu de opções irá aparecer no canto esquerdo de sua dashboard, acesse a opção de “Sinistro”. Você será redirecionado a uma página onde você deve preencher todos os dados sobre o ocorrido corretamente, além de anexar informações cruciais como fotos e boletim de ocorrência. </p>
               <p>Após o preenchimento, você será redirecionado a uma nova página onde estará informado o número do protocolo e o status do chamado efetuado.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Como acompanho o status da resolução do meu sinistro ? <i class="ion-android-remove"></i></a>
            <div id="faq4" class="collapse" data-parent="#faq-list">
              <p>
              Após efetuar o login, o menu de opções irá aparecer no canto esquerdo de sua dashboard, acesse a opção de “Sinistro”. Você será redirecionado a uma página onde você deve acessar a opção “Status”. Uma tabela com data, protocolo e o status do chamado irá aparecer informando o estado do chamado.              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Como acompanho o período restante de seguro do meu bem ? <i class="ion-android-remove"></i></a>
            <div id="faq5" class="collapse" data-parent="#faq-list">
              <p>
                Após efetuar o login, em sua dashboard aparecerá um contador com o período de tempo restante de seu bem assegurado.

                No menu a esquerda em sua dashboard na opção “Itens assegurados” você poderá acompanhar de forma completa os bens assegurados.              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq6" class="collapsed">Como prolongo o período de seguro do meu bem ? <i class="ion-android-remove"></i></a>
            <div id="faq6" class="collapse" data-parent="#faq-list">
              <p>
                Após efetuar o login, o menu de opções irá aparecer no canto esquerdo de sua dashboard, acesse a opção de “Itens Assegurados”, após entrar na opção você será redirecionado em um página mostrando o status de cada item assegurado. </p>
                <p> Abaixo de cada item se encontra a opção “Prolongar”, um novo campo irá aparecer onde você deve preencher o período que você gostaria de prolongar, após preenchimento conclua e você será redirecionado para página de pagamento. </p>
            </div>
          </li>

        </ul>

      </div>
    </section><!-- #faq -->

    <!--==========================
      Our Team Section
    ============================-->
    <section id="team" class="section-bg">
      <div class="container">
        <div class="section-header">
          <h3 class="section-title">Equipe</h3>
          <span class="section-divider"></span>
          <p class="section-description">Colaboradores por trás da idéia.</p>
        </div>
        <div class="row wow fadeInUp">
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-1.jpg" alt=""></div>
              <h4>Igor Siqueirá</h4>
              <span>Software Engineer</span>
              <div class="social">
                <a href= "https://www.facebook.com/igor.i.siqueirá" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/in/igor-siqueirá-4336b7a3/" target="_blank"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-2.png" alt=""></div>
              <h4>Igor Richard</h4>
              <span>Product Manager</span>
              <div class="social">
               
                <a href="https://www.facebook.com/igor.richard.58"target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/in/igor-richard-ba3a1598/" target="_blank"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-5.jpg" alt=""></div>
              <h4>David Allan Cordeiro</h4>
              <span>Full Developer</span>
              <div class="social">
               
                <a href="https://www.facebook.com/david.allan.180072"target="_blank"><i class="fa fa-facebook"></i></a>                
                <a href="https://www.linkedin.com/in/davidallancordeiro/" target="_blank"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-4.jpg" alt=""></div>
              <h4>Emerson Meirá</h4>
              <span>Marketing Analyst</span>
              <div class="social">
                <a href= "https://www.facebook.com/emerson.meirá.1" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/in/emerson-marcos-moreirá-meirá-51ab26aa/" target="_blank"><i class="fa fa-linkedin"></i></a>              </div>
            </div>
          </div>
          <div class="col-lg-2 col-md-4">
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-3.jpg" alt=""></div>
              <h4>Silas Teixeirá</h4>
              <span>Marketing Analyst</span>
              <div class="social">
                <a href= "https://www.facebook.com/SilasTP" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/in/silas-teixeirá-83263ba1/" target="_blank"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="landpage/img/team/team-6.jpg" alt=""></div>
              <h4>Mick Douglas</h4>
              <span>Product Manager</span>
              <div class="social">
                <a href= "https://www.facebook.com/mick.douglas.9" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/in/mick-xavier-da-cruz-118572101/" target="_blank"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

        <div class="col-lg-3 col-md-6">
          <div class="member">
            <div class="pic"><img src="landpage/img/team/team-7.jpg" alt=""></div>
            <h4>Juliana Nonaka</h4>
            <span>Designer</span>
            <div class="social">
              <a href= "https://www.facebook.com/zuh.non" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="https://www.linkedin.com/in/mick-xavier-da-cruz-118572101/" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
          </div>
        </div>
      </div>

      </div>
    </section><!-- #team -->
    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3>TempSeguros</h3>
              <p>A melhor soluçao de seguros para voce.</p>
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>Rua Sao Paulo , N°447 - Centro Belo Horizonte/MG</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>contato@tempseguros.com.br</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>(31)3436-2147</p>
              </div>

            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Enviar</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>TempSeguros</strong>. All Rights Reserved
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Inicio</a>
            <a href="#about" class="scrollto">Sobre Nos</a>
            <a href="#">Politica de Privacidade</a>
            <a href="#">Termos de Uso</a>
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{asset('landpage/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('landpage/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('landpage/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('landpage/lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('landpage/lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('landpage/lib/superfish/hoverIntent.js')}}"></script>
  <script src="{{asset('landpage/lib/superfish/superfish.min.js')}}"></script>
  <script src="{{asset('landpage/lib/magnific-popup/magnific-popup.min.js')}}"></script>


  <!-- Template Main Javascript File -->
  <script src="{{asset('landpage/js/main.js')}}"></script>


</body>
</html>

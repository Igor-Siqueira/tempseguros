@extends('layouts.app')


@section('content')

   @if (session('status'))
   
        {{ session('status') }}
   
    @endif

    <?php 
        $idAssured = "";
        $Name = "";
        $Cpf = "";
        $PhoneFix = "";
        $PhoneCel = "";
        $Cep = "";
        $Street = "";
        $Number = "";
        $Neighborhood = "";
        $City = "";
        $State = "";
        $Complement = "";

        $Printname = "";      
        $Creditnumber = "";
        $Securecode = "";
        $Datevalidate = "";
        $Printname = "";
        $Creditnumber = "";
        $Securecode = "";
        $Datevalidate = "";
       
    ?>
    <script type="text/javascript">
    $(document).ready(function() {
       
        $("#TitlePage").html("Perfil do Assegurado");
        $("#DescriptionPage").html("Preencha os dados para começar a utilizar o serviço.");
        $( "#PerfiAssured" ).addClass( "active" );

        $("#AssuredForm").validate({
			rules: {
				Name: {
					required: true,
					minlength: 2
				},
				Cpf: {
					required: true,
					minlength: 11
                   
				},
				PhoneCel: {
					required: true,
					minlength: 14
				},
				Cep: {
					required: true,
                    minlength: 10,
				},
                Street: {
					required: true
                    
				},
                Number: {
					required: true,
                    number: true
				},
				Neighborhood: {
					required: true
				},
				City: {
					required: true
				},
                State: {
					required: true
				},
                Printname: {
					required: true
				},
                Creditnumber: {
					required: true,
                    minlength: 16,
                    number : true
				},
                Securecode: {
					required: true,
                    minlength: 3,
                    number : true
				},
                Datevalidate: {
					required: true,
                    minlength: 7
                   
				}
			},
			messages: {
				Name: {
					required: "Digite seu nome.",
					minlength: "Nome deve conter no mínimo 2 caracteres"
				},
				Cpf: {
					required: "Digite o CPF",
					minlength: "CPF deve conter 13 caracteres"
				},
				PhoneCel: {
					required: "Digite um telefone celular",
					minlength: "Celular deve conter 14 caracteres",
					
				},
				Cep: {
                  required: "Preencha o cep",
                  minlength : "CEP deve conter 10 caracteres"
                },
                Street: {
                  required: "Preencha o logradouro"
                },
                 Number: {
					required: "Preencha o numero",
                    number: "Somente número"
				},
                Neighborhood: {
					required: "Preencha o Bairro"
                   
				},
                City: {
					required: "Preencha o Cidade"
                   
				},
                State: {
					required: "Preencha o Estado"
                   
				},
                Printname: {
					required: "Preencha o nome impresso"
                   
				},
                Creditnumber: {
					required: "Preencha o número do cartão",
                    minlength: "O número deve conter 16 digitos",
                    number : "Preencha com somente números"
				},
                Securecode: {
					required: "Preencha o código de segurança",
                    minlength: "O número deve conter 3 digitos",
                    number : "Preencha com somente números"
				},
                Datevalidate: {
					required: "Preencha a data de validade",
                    minlength: "O número deve conter 7 digitos"
                   
				}
			}
		});
    });
</script>
 <div class="row">
    <div class="col-lg-10">
        <form role="form" action="{{url("assured/save")}}" id="AssuredForm" method="post">
            {{csrf_field()}}
            <input type="hidden" name="idAssured" id="idAssured" value="<?php echo $idAssured?>">
            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-user fa-2x"></i> <strong> Dados Básico</strong>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Name">Nome Completo</label>
                        <input type="text" class="form-control" name="Name"  id="Name"  value="<?php echo $Name?>" placeholder="Nome Completo">
                    </div>
                    <div class="form-group">
                        <label for="Cpf">CPF</label>
                        <input type="text" class="form-control cpf"  name="Cpf" id="Cpf" value="<?php echo $Cpf?>" placeholder="CPF">
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-phone fa-2x"></i><strong> Contatos</strong>
                </header>
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="PhoneFix">Telefone Fixo</label>
                            <input type="text" class="form-control TelFix" name="PhoneFix" id="PhoneFix" value="<?php echo $PhoneFix?>" placeholder="(00)0000-0000">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="PhoneCel">Telefone Celular</label>
                            <input type="text" class="form-control TelCel" name="PhoneCel" id="PhoneCel" value="<?php echo $PhoneCel?>"  placeholder="(00)00000-0000">
                        </div>
                    </div>
                <div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-map-marker fa-2x"></i><strong> Endereço</strong>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="Cep">CEP</label>
                            <input type="text" class="form-control cep" id="Cep" name="Cep" value="<?php echo $Cep?>" placeholder="00.000-00">
                        </div>
                        <div class="col-md-9">
                            <label for="Street">Logradouro</label>
                            <input type="text" class="form-control" name="Street" id="Street"  value="<?php echo $Street?>" placeholder="Rua,Av,Street">
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Number">Número</label>
                            <input type="number" class="form-control" name="Number" id="Number" value="<?php echo $Number?>" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="Neighborhood">Bairro</label>
                            <input type="text" class="form-control" name="Neighborhood" id="Neighborhood" value="<?php echo $Neighborhood?>" placeholder="Bairro">
                        
                        </div>
                        <div class="col-md-3">
                            <label for="City">Cidade</label>
                            <input type="text" class="form-control" name="City" id="City" value="<?php echo $City?>" placeholder="Cidade">
                        
                        </div>
                        <div class="col-md-3">
                            <label for="State">Estado</label>
                            <input type="text" class="form-control"  name="State" id="State" value="<?php echo $State?>" placeholder="Bairro">
                        
                        </div>
                        <div class="col-md-6">
                            <label for="Complement">Complemento</label>
                            <input type="text" class="form-control" name="Complement" id="Complement" value="<?php echo $Complement?>" placeholder="Apartamento 10,Bloco 2">
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-credit-card fa-2x"></i><strong> PAGAMENTO</strong>
                </header>
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Printname">Nome Impresso</label>
                            <input type="text" class="form-control" name="Printname" id="Printname" value="<?php echo $Complement?>" placeholder="Nome impresso">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Creditnumber">Número do Cartão</label>
                            <input type="text" class="form-control" name="Creditnumber" id="Creditnumber" placeholder="0000 0000 0000 0000">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Securecode">Código Segurança</label>
                            <input type="text" class="form-control" name="Securecode" id="Securecode" placeholder="000" maxlength="3" minlength="3">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Datevalidate">Data Validade</label>
                            <input type="text" class="form-control DateBank" name="Datevalidate" id="Datevalidate" placeholder="00/0000" maxlength="8" minlength="8">
                        </div>
                    </div>
                <div>
                </div>
            </section>
            <button type="submit" class="btn btn-info">Salvar</button><br>
        </form>
    </div>
</div>
 <br>           
@endsection


@extends('layouts.loginLand')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="Admin Template">
            <meta name="keywords" content="admin dashboard, admin, flat, flat ui, ui kit, app, web app, responsive">
            <link rel="shortcut icon" href="img/ico/favicon.png">
            <title>TempSeguros - Registre-se</title>

            <!-- Base Styles -->
            <link href="{{ asset('software/css/style.css') }}" rel="stylesheet">
            <link href="{{ asset('software/css/style-responsive.css') }}" rel="stylesheet">
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="{{ asset('software/js/html5shiv.min.js') }}"></script>
            <script src="{{ asset('software/js/respond.min.js') }}"></script>
            <![endif]-->
        </head>

        <body class="login-body">

            <div class="login-logo">
                <img src="{{asset('software/img/logo2.png')}}"  alt="TempSeguros"><br><h3><strong>TempSeguros</strong></h3>
            </div>

            <h2 class="form-heading">Cadastrar</h2>
            <div class="container log-row">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Nome Completo</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-mail</label>
                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Senha</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirmaçao Senha</label>
                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Registrar
                        </button>
                        <button type="button" id="backHome" class="btn btn-default ">
                            Voltar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!--jquery-1.10.2.min-->
        <script src="{{ asset('software/js/jquery-1.11.1.min.js') }}"></script>
        <!--Bootstrap Js-->
        <script src="{{ asset('software/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('software/js/jrespond..min.js') }}"></script>
        </body>
        <script>
            $( document ).ready(function() {
               $('#backHome').click(function(){
                    window.location.href='/';
                })
            });
        </script>
    </html>
@endsection

@extends('layouts.app')


@section('content')

   @if (session('status'))
   
        {{ session('status') }}
   
    @endif
    
    <script type="text/javascript">
    $(document).ready(function() {
        $("#TitlePage").html("Ativações Seguro");
        $("#DescriptionPage").html("Visualize,ative e desative seu seguro.");
        $( "#InsurancePolicy" ).addClass( "active" );
    });
</script>

<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            SEGURO ATIVOS
            <span class="tools pull-right">
                <a class="t-collapse fa fa-chevron-down" href="javascript:;"> </a>
                <a class="" href="#"></a>
                <button type="button"  onclick="location.href = '{{ url('/insurance/create') }}';" class="btn btn-round btn-success">Criar Novo Seguro</button>
            </span>
        </header>
        <div class="panel-body">
            <ul class="todo-list-item" id="todo-list">
                <li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            Descriçao : Igor Siqueira
                        </div>
                        <div class="col-sm-12 col-md-3">
                            Inicio :   22/05/2010
                        </div>
                        <div class="col-sm-12 col-md-3">
                            Fim :   22/05/2010
                        </div>
                        <div class="col-sm-12 col-md-3">
                                <spam> Status : Ativo</spam>
                        </div>
                        <div class="col-md-12 col-sm-1 no-padding">
                            &nbsp;
                        </div>
                        <div class="col-sm-12">
                            <input type="checkbox" class="js-switch-small-green"  data-switchery="false" style="display: none;">
                            <button type="button" class="btn btn-sm btn-round btn-danger">Excuir</button>
                        </div>
                    </div>
                </li>
            </ul>

            <ul class="todo-list-item" id="todo-list">
                <li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            Descriçao : Igor Siqueira
                        </div>
                        <div class="col-sm-12 col-md-3">
                            Inicio :   22/05/2010
                        </div>
                        <div class="col-sm-12 col-md-3">
                            Fim :   22/05/2010
                        </div>
                        <div class="col-sm-12 col-md-3">
                                <spam> Status : Ativo</spam>
                        </div>
                        <div class="col-md-12 col-sm-1 no-padding">
                            &nbsp;
                        </div>
                        <div class="col-sm-12">
                            <input type="checkbox" class="js-switch-small-green"  data-switchery="false" style="display: none;">
                            <button type="button" class="btn btn-sm btn-round btn-danger">Excuir</button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
</div>
@endsection


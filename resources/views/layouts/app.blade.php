<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Mosaddek" />
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>TempSeguros - DashBoard</title>

    <!-- Placed js at the end of the document so the pages load faster -->
<script src="{{asset('software/js/jquery-1.10.2.min.js')}}"></script>


<!--jquery-ui-->
<script src="{{asset('software/js/jquery-ui/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>


    <!--easy pie chart-->
    <link href="{{ asset('software/js/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen" />

    <!--vector maps -->
    <link rel="stylesheet" href="{{ asset('software/js/vector-map/jquery-jvectormap-1.1.1.css') }}">

    <!--right slidebar-->
    <link href="{{ asset('software/css/slidebars.css') }}" rel="stylesheet">

    <!--switchery-->
    <link href="{{ asset('software/js/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />

    <!--jquery-ui-->
    <script src="{{ asset('software/js/jquery.validate.min.js') }}"></script>
    
     <script src="{{ asset('software/js/jquery.mask.min.js') }}"></script>
    <!--jquery-ui-->
    <link href="{{ asset('software/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet" />
   
    <!--iCheck-->
    <link href="{{ asset('software/js/icheck/skins/all.css') }}" rel="stylesheet">

    <link href="{{ asset('software/css/owl.carousel.css') }}" rel="stylesheet">
 
    <!--common style-->
    <link href="{{ asset('software/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('software/css/style-responsive.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('software/js/html5shiv.js') }}"></script>
    <script src="{{ asset('software/js/respond.min.js') }}"></script>
    <![endif]-->
</head>

<body class="sticky-header">

    <section>
        <!-- sidebar left start-->
        <div class="sidebar-left">
            <!--responsive view logo start-->
            <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('software/img/logo2.png') }}" alt="">
                   
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">TempSeguros</span>
                </a>
            </div>
            <!--responsive view logo end-->
 
 
            <div class="sidebar-left-info">
                <!-- visible small devices start-->
                <div class=" search-field">  </div>
                <!-- visible small devices end-->

                <!--sidebar nav start-->
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Seguros e Ativações</h3>
                         
                    </li>
                    <li  id="DashboardMenu" class=""><a href="{{ route('home') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    <li  id="InsurancePolicy" class="opçoeActive"><a href="{{ url('/insurance') }}"><i class="fa fa-lock"></i> <span>Ativar Seguro</span></a></li>
                
                    <li>
                        <h3 class="navigation-title">Configurações e Dados</h3>
                    </li>
                   <li  id="PerfiAssured" class="opçoeActive"><a id="PerfilAssuredLink" href="{{ url('/assured') }}"><i class="fa fa-user"></i> <span>Perfil do Assegurado</span></a></li>
                   <li id="BensApp" class="opçoeActive"><a id="EstateLink" href="{{ url('/estate') }}"><i class="fa fa-list-ul"></i> <span>Bens</span></a></li>

                    

                   
                    <li>
                        <h3 class="navigation-title">Extra</h3>
                         <li class="opçoeActive"><a href="{{ route('home') }}"><i class="fa fa-exclamation-circle"></i> <span>Ocorrência</span></a></li>
                    </li>

                </ul>
                <!--sidebar nav end-->

                
            </div>
        </div>
        <!-- sidebar left end-->

        <!-- body content start-->
        <div class="body-content" >

            <!-- header section start-->
            <div class="header-section">

                <!--logo and logo icon start-->
                <div class="logo dark-logo-bg hidden-xs hidden-sm">
                    <a href="{{ route('home') }}">
                        <img src="{{asset('software/img/logo2.png')}}" height="50" width="50" alt="TempSeguros">TempSeguros
                    </a>
                </div>

                <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                   <a href="{{ route('home') }}">
                        <img src="{{asset('software/img/logo2.png')}}" height="50" width="50" alt="TempSeguros">
                    </a>
                </div>
                <!--logo and logo icon end-->

                <!--toggle button start-->
                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                <!--toggle button end-->

                <!--mega menu start-->
                <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                
                </div>
                <!--mega menu end-->
                <div class="notification-wrap">
                <!--left notification start-->
                <div class="left-notification">
                <ul class="notification-menu"   
                <!--notification info start-->
                <li>
                    <a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge bg-warning">4</span>
                    </a>

                    <div class="dropdown-menu dropdown-title ">

                        <div class="title-row">
                            <h5 class="title yellow">
                                Você tem 2 notificações
                            </h5>
                            <a href="javascript:;" class="btn-warning btn-view-all">Ver</a>
                        </div>
                        <div class="notification-list-scroll sidebar">
                            <div class="notification-list mail-list not-list">
                                <a href="javascript:;" class="single-mail">
                                    <span class="icon bg-primary">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <strong>Novas funcionalidades</strong>

                                    <p>
                                        <small>Agora</small>
                                    </p>
                                    <span class="un-read tooltips" data-original-title="Mark as Read" data-toggle="tooltip" data-placement="left">
                                        <i class="fa fa-circle"></i>
                                    </span>
                                </a>
                                <a href="javascript:;" class="single-mail">
                                    <span class="icon bg-success">
                                        <i class="fa fa-comments-o"></i>
                                    </span>
                                    <strong>Retorno Ocorrência</strong>

                                    <p>
                                        <small>30 minutos</small>
                                    </p>
                                    <span class="un-read tooltips" data-original-title="Mark as Read" data-toggle="tooltip" data-placement="left">
                                        <i class="fa fa-circle"></i>
                                    </span>
                                </a> 
                            </div>
                        </div>
                    </div>
                </li>
                <!--notification info end-->
                </ul>
                </div>
                <!--left notification end-->


                <!--right notification start-->
                <div class="right-notification">
                    <ul class="notification-menu">
                        <li>
                            <form class="search-content" action="{{ route('home') }}" method="post">
                                <input type="text" class="form-control" name="keyword" placeholder="Buscar...">
                            </form>
                        </li>

                        <li>
                            <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="img/avatar-mini.jpg" alt="">{{ Auth::user()->name }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                <li><a href="javascript:;">Perfil</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-danger pull-right">40%</span>
                                        <span>Configurações</span>
                                    </a>
                                   
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="label bg-info pull-right">new</span>
                                        Suporte
                                    </a>
                                </li>
                                <li>
                                     <a 
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        

                    </ul>
                </div>
                <!--right notification end-->
                </div>

            </div>
            <!-- header section end-->

            <!-- page head start-->
            <div class="page-head">
                <h3 id="TitlePage">
                   
                </h3>
                <span  id="DescriptionPage" class="sub-title"></span>
               
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
            <style>
                .error{
                    color: red;
                }
            </style>

@yield('content')




 </div>
            <!--body wrapper end-->


            <!--footer section start-->
            <footer>
                2018 &copy; TempSeguros.
            </footer>
            <!--footer section end-->


            
        </div>
        <!-- body content end-->
    </section>





<script src="{{asset('software/js/jquery-migrate.js')}}"></script>
<script src="{{asset('software/js/bootstrap.min.js')}}"></script>
<script src="{{asset('software/js/modernizr.min.js')}}"></script>





<!--right slidebar-->
<script src="{{asset('software/js/slidebars.min.js')}}"></script>


<!--switchery-->
<script src="{{asset('software/js/switchery/switchery.min.js')}}"></script>
<script src="{{asset('software/js/switchery/switchery-init.js')}}"></script>


<!--flot chart -->
<script src="{{asset('software/js/flot-chart/jquery.flot.js')}}"></script>
<script src="{{asset('software/js/flot-chart/flot-spline.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.resize.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.pie.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.selection.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.stack.js')}}"></script>
<script src="{{asset('software/js/flot-chart/jquery.flot.crosshair.js')}}"></script>


<!--earning chart init-->
<script src="{{asset('software/js/earning-chart-init.js')}}"></script>

<!--Sparkline Chart-->
<script src="{{asset('software/js/sparkline/jquery.sparkline.js')}}"></script>
<script src="{{asset('software/js/sparkline/sparkline-init.js')}}"></script>



<!--easy pie chart-->
<script src="{{asset('software/js/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
<script src="{{asset('software/js/easy-pie-chart.js')}}"></script>



<!--vectormap-->
<script src="{{asset('software/js/vector-map/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('software/js/vector-map/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('software/js/dashboard-vmap-init.js')}}"></script>


<!--Icheck-->
<script src="{{asset('software/js/icheck/skins/icheck.min.js')}}"></script>
<script src="{{asset('software/js/todo-init.js')}}"></script>


<!--jquery countTo-->
<script src="{{asset('software/js/jquery-countTo/jquery.countTo.js')}}"  type="text/javascript"></script>


<!--owl carousel-->
<script src="{{asset('software/js/owl.carousel.js')}}"></script>


<!--common scripts for all pages-->

<script src="{{asset('software/js/scripts.js')}}"></script>


<script type="text/javascript">

    $(document).ready(function() {

        <?php if(session("idAssured") != "0"){?>
               $("#PerfilAssuredLink").attr("href", "{{ url('assured/edit/'.session("idAssured")) }}")
        <?php } ?>
        $('.timer').countTo();

        $('.cep').mask('00.000-000');
        $('.cpf').mask('000.000.000-00');
        $('.TelFix').mask('(00)0000-0000');
        $('.TelCel').mask('(00)00000-0000');
        $('.DateBank').mask('00/0000');
        $('.Car').mask('AAA-0000');
        $('.CarRenavan').mask('00000000000');
        $('.CarYear').mask('0000');

        //owl carousel

        $("#news-feed").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });
</script>

</body>
</html>


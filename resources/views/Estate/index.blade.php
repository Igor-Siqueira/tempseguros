@extends('layouts.app')

@section('content')

   @if (session('status'))
   
        {{ session('status') }}
   
    @endif
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#TitlePage").html("Bens do Assegurado");
            $("#DescriptionPage").html("Cadastre os itens que você deseja assegurar.");
            $( "#BensApp" ).addClass( "active" );
        });
    </script>

    <div class="form-group">
        <a href="{{url('estate/create')}}" class="btn btn-primary" role="button"> Cadastrar Bem </a>
    </div>

  
        @foreach($objEstate as $estateList)
        <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading head-border">
                            Lista de Bens
                            <span class="tools pull-right">
                                <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="noti-information notification-menu">
                            <!--notification info start-->
                            <div class="notification-list mail-list not-list">
                                <a href="{{url('estate/edit', $estateList->idEstate)}}" class="single-mail">
                                        <span class="icon bg-primary">
                                            <i class="fa fa-car"></i>
                                        </span>
                                    <span class="purple-color"> <?php echo $estateList->Name ?></span>
                                    <p>
                                        <small><?php echo $estateList->Description ?></small>
                                    </p>
                                        <span class="read tooltips" data-original-title="Mark as Unread" data-toggle="tooltip" data-placement="left">
                                            <i class="fa fa-circle-o"></i>
                                        </span>
                                </a>
                                <a href="javascript:;" class="single-mail text-center">
                                   Ver mais!
                                </a>

                            </div>
                            <!--notification info end-->
                        </div>
                    </section>
                </div>
                
                </div>
        @endforeach
@endsection
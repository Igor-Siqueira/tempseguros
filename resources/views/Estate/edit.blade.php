@extends('layouts.app')

@section('content')

   @if (session('status'))
   
        {{ session('status') }}
   
    @endif
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#TitlePage").html("Edição dos bens do Assegurado");
            $("#DescriptionPage").html("Edite os dados do item selecionado.");
            $( "#PerfiAssured" ).addClass( "active" );

            $("#formEstate").validate({

            });
        });
    </script>
 <div class="row">
    <div class="col-lg-10">

      
       
        <form role="form" id="formEstate" action="{{ url('estate/save')}}" method="POST">
            {{csrf_field()}}

            <input class = "form-control" type="hidden" name="SelectedCategory" value="{{ $objEstateEdit->idCategoryEstate }}">
            <input class = "form-control" type="hidden" name="idEstate" value="{{ $objEstateEdit->idEstate }}">

            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-user fa-2x"></i> <strong> Dados Basicos</strong>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Name"> Nome do Bem </label>
                        <input type="text" class="form-control" name="Name" id="Name" value="{{ $objEstateEdit->Name }}">
                    </div>
                    <div class="form-group">
                        <label for="Modelo">Modelo</label>
                        <input type="text" class="form-control" name="Modelo" id="Modelo" value="{{ $objEstateEdit->Modelo }}">
                    </div>
                    <div class="form-group">
                        <label for="Description">Descrição</label>
                        <input type="text" class="form-control" name="Description" id="Description" value="{{ $objEstateEdit->Description }}">
                    </div>                    
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-car fa-2x"></i><strong> Dados do Veículo</strong>
                </header>
                
                <div class="panel-body">                
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Placa"> Placa do Veículo </label>
                                <input type="text" class="form-control Car" name="Placa" id="Placa" value="{{ $objEstateEdit->Placa }}">
                            </div>
                        </div>                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Renavam"> Número do Renavam </label>
                                <input type="text" class="form-control CarRenavan" name="Renavam" id="Renavam" value="{{ $objEstateEdit->Renavam }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Chassi"> Número do Chassi </label>
                                <input type="text" class="form-control" name="Chassi" id="Chassi" value="{{ $objEstateEdit->Chassi }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Color"> Cor do Veículo </label>
                                <input type="text" class="form-control" name="Color" id="Color" value="{{ $objEstateEdit->Color }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="Year"> Ano do Veículo </label>
                                <input type="text" class="form-control CarYear" name="Year" id="Year" value="{{ $objEstateEdit->Year }}">
                            </div>
                        </div>                
                    </div>
                
                </div>

            </section>

            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-map-marker fa-2x"></i><strong> Imóveis</strong>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="Cep">CEP</label>
                            <input type="text" class="form-control cep" name='Cep' id="Cep" value="{{ $objEstateEdit->Cep }}">
                        </div>
                        <div class="col-md-9">
                            <label for="Street">Logradouro</label>
                            <input type="text" class="form-control" name='Street' id="Street" value="{{ $objEstateEdit->Street }}">                        
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Number">Número</label>
                            <input type="Number" class="form-control" name='Number' id="Number" value="{{ $objEstateEdit->Number }}">
                        </div>
                        <div class="col-md-4">
                            <label for="Neighborhood">Bairro</label>
                            <input type="text" class="form-control" name='Neighborhood' id="Neighborhood" value="{{ $objEstateEdit->Neighborhood }}">
                        </div>
                        <div class="col-md-3">
                            <label for="City">Cidade</label>
                            <input type="text" class="form-control" name='City' id="City" value="{{ $objEstateEdit->City }}">
                        </div>
                        <div class="col-md-3">
                            <label for="State">Estado</label>
                            <input type="text" class="form-control" name='State' id="State" value="{{ $objEstateEdit->State }}">
                        </div>
                        <div class="col-md-3">
                            <label for="Dimensions">Dimensões M2</label>
                            <input type="text" class="form-control" name='Dimensions' id="Dimensions" value="{{ $objEstateEdit->Dimensions }}">
                        </div>
                        <div class="col-md-4">
                            <label for="Complement">Complemento</label>
                            <input type="text" class="form-control" name='Complement' id="Complement" value="{{ $objEstateEdit->Complement }}">
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-bolt fa-2x"></i><strong> Dados do Eletrónico</strong>
                </header>
                <div class="panel-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Serial"> Número de série do Eletrónico </label>
                            <input type="text" class="form-control" name="Serial" id="Serial" value="{{ $objEstateEdit->Serial }}">
                        </div>
                    </div>

                <div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-mobile fa-2x"></i><strong> Dados do Smartphone</strong>
                </header>
                <div class="panel-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Imei"> Número do Imei do Smartphone </label>
                            <input type="text" class="form-control" name="Imei" id="Imei" value="{{ $objEstateEdit->Imei }}">
                        </div>
                    </div>

                <div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-camera-retro fa-2x"></i><strong> Dado Fotográfico </strong>
                </header>
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UrlPhoto"> Photo do bem segurado </label>
                            <input type="text" class="form-control" name="UrlPhoto" id="UrlPhoto" value="{{ $objEstateEdit->UrlPhoto }}">
                        </div>
                    </div>
                <div>
                </div>
            </section>

            <button type="submit" class="btn btn-info">Salvar</button><br>
      
        </form>
        
    </div>
</div>
 <br>           
@endsection


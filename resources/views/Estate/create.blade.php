@extends('layouts.app')

@section('content')

   @if (session('status'))
   
        {{ session('status') }}
   
    @endif
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#TitlePage").html("Bens do Assegurado");
            $("#DescriptionPage").html("Cadastre os itens que você deseja assegurar.");
            $( "#PerfiAssured" ).addClass( "active" );

            $("#formEstate").validate({

            });
        });
    </script>
 <div class="row">
    <div class="col-lg-10">

        <form role="form" id="formEstate" action="{{ url('estate/save')}}" method="POST">
            {{csrf_field()}}

            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-list-ul fa-2x"></i> <strong> Selecione a categoria do bem</strong>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control" name="SelectedCategory" id="SelectedCategory">
                            @foreach($objCategoryEstate as $categoryEstateList)
                                <option value={{ $categoryEstateList->idCategoryEstate }}> {{ $categoryEstateList->Name }} </option>
                            @endforeach 
                        </select>
                    </div>  
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-user fa-2x"></i> <strong> Dados Basicos</strong>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Name">Nome do Bem</label>
                        <input type="text" class="form-control" name="Name" id="Name" placeholder="Nome do bem">
                    </div>
                    <div class="form-group">
                        <label for="Modelo">Modelo</label>
                        <input type="text" class="form-control" name="Modelo" id="Modelo" placeholder="Modelo">
                    </div>
                    <div class="form-group">
                        <label for="Description">Descrição</label>
                        <input type="text" class="form-control" name="Description" id="Description" placeholder="Descrição">
                    </div>                    
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-car fa-2x"></i><strong> Dados do Veículo</strong>
                </header>
                
                <div class="panel-body">                
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Placa"> Placa do Veículo </label>
                                <input type="text" class="form-control Car" name="Placa" id="Placa" placeholder="Placa do veículo">
                            </div>
                        </div>                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Renavam"> Número do Renavam </label>
                                <input type="text" class="form-control CarRenavan" name="Renavam" id="Renavam" placeholder="Renavam">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Chassi"> Número do Chassi </label>
                                <input type="text" class="form-control" name="Chassi" id="Chassi" placeholder="Chassi">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Color"> Cor do Veículo </label>
                                <input type="text" class="form-control" name="Color" id="Color" placeholder="Cor do veículo">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="Year"> Ano do Veículo </label>
                                <input type="text" class="form-control CarYear" name="Year" id="Year" placeholder="Ano do veículo">
                            </div>
                        </div>                
                    </div>
                
                </div>

            </section>

            <section class="panel">
                <header class="panel-heading">
                    <i class="fa fa-map-marker fa-2x"></i><strong> Imóveis</strong>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="Cep">CEP</label>
                            <input type="text" class="form-control cep" name='Cep' id="Cep" placeholder="00.000-00">
                        </div>
                        <div class="col-md-9">
                            <label for="Street">Logradouro</label>
                            <input type="text" class="form-control" name='Street' id="Street" placeholder="Rua, Av.">                        
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="Number">Número</label>
                            <input type="Number" class="form-control" name='Number' id="Number" placeholder="Número">
                        </div>
                        <div class="col-md-4">
                            <label for="Neighborhood">Bairro</label>
                            <input type="text" class="form-control" name='Neighborhood' id="Neighborhood" placeholder="Bairro">
                        </div>
                        <div class="col-md-3">
                            <label for="City">Cidade</label>
                            <input type="text" class="form-control" name='City' id="City" placeholder="Cidade">
                        </div>
                        <div class="col-md-3">
                            <label for="State">Estado</label>
                            <input type="text" class="form-control" name='State' id="State" placeholder="Estado">
                        </div>
                        <div class="col-md-3">
                            <label for="State">Dimensões M2</label>
                            <input type="text" class="form-control" name='Dimensions' id="Dimensions" placeholder="30m2">
                        </div>
                        <div class="col-md-4">
                            <label for="Complement">Complemento</label>
                            <input type="text" class="form-control" name='Complement' id="Complement" placeholder="Estado">
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-bolt fa-2x"></i><strong> Dados do Eletrónico</strong>
                </header>
                <div class="panel-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Serial"> Número de série do Eletrónico </label>
                            <input type="text" class="form-control" name="Serial" id="Serial" placeholder="Número de série do Elétronico">
                        </div>
                    </div>

                <div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-mobile fa-2x"></i><strong> Dados do Smartphone</strong>
                </header>
                <div class="panel-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Imei"> Número do Imei do Smartphone </label>
                            <input type="text" class="form-control" name="Imei" id="Imei" placeholder="Número do Imei do Elétronico">
                        </div>
                    </div>

                <div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                   <i class="fa fa-camera-retro fa-2x"></i><strong> Dado Fotográfico </strong>
                </header>
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UrlPhoto"> Photo do bem segurado </label>
                            <input type="text" class="form-control" name="UrlPhoto" id="UrlPhoto" placeholder="Url da Imagem">
                        </div>
                    </div>
                <div>
                </div>
            </section>

            <button type="submit" class="btn btn-info">Salvar</button><br>
        
        </form>
    </div>
</div>
 <br>           
@endsection


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryEstateModel extends Model
{
    protected $fillable = [
        'idCategoryEstate',
        'Name',
        'created_at',
        'updated_at'
    ];

    protected $table = 'CategoryEstate';
    protected $primaryKey = 'idCategoryEstate';

}
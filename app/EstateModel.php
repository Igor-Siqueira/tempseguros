<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstateModel extends Model
{

    protected $fillable = [
        'idEstate',  
        'Name',
        'iduser',
        'UrlPhoto',
        'idCategoryEstate',
        'Modelo',
        'Description',
        'Placa',
        'Renavam',
        'Chassi',
        'Color',
        'Year',
        'Serial',
        'Imei',
        'State',
        'City',
        'Street',
        'Neighborhood',
        'Number',
        'Cep',
        'Complement',
        'Dimensions',
        'created_at',
        'updated_at'
    ];

    protected $table = 'Estate';
    protected $primaryKey = 'idEstate';

    public static function FindEstateUser() {
        return static::where('iduser', session("iduser"));
    }
    public static function FindEstatebyId($idEstate){

        return static::where('idEstate', $idEstate);
    }

}
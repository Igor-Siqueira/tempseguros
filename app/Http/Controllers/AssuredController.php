<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Crypt;
use App\AssuredModel;

class AssuredController extends Controller
{
    public function create()
    {
        return view('Assured.create');
    }
    
    public function edit($id)
    {
        $idAssured = $id;
        $objAssuredAll = AssuredModel::FindAssured($idAssured)->get();
        foreach ($objAssuredAll as &$objAssured) {

            $objAssuredView =  new AssuredModel();

            $objAssuredView->idAssured = $objAssured->idAssured;
            $objAssuredView->Name =  $objAssured->Name;
            $objAssuredView->Cpf = $objAssured->Cpf; 
            $objAssuredView->PhoneFix = $objAssured->PhoneFix;
            $objAssuredView->PhoneCel = $objAssured->PhoneCel;
            $objAssuredView->Cep = $objAssured->Cep; 
            $objAssuredView->Street = $objAssured->Street;
            $objAssuredView->Number = $objAssured->Number;
            $objAssuredView->Neighborhood = $objAssured->Neighborhood;
            $objAssuredView->City = $objAssured->City;
            $objAssuredView->State = $objAssured->State;
            $objAssuredView->Complement = $objAssured->Complement;
            $objAssuredView->Printname = Crypt::decrypt($objAssured->Printname);
            $objAssuredView->Creditnumber = Crypt::decrypt($objAssured->Creditnumber);
            $objAssuredView->Securecode = Crypt::decrypt($objAssured->Securecode);
            $objAssuredView->Datevalidate = Crypt::decrypt($objAssured->Datevalidate);
           
        }
        return view('Assured.edit')->with(['objAssured'=>$objAssuredView]);
    }

    public function save()
    {
       $idAssured = request("idAssured");
       $Name = request("Name");
       $Cpf = request("Cpf");
       $PhoneFix = request("PhoneFix");
       $PhoneCel = request("PhoneCel");
       $Cep = request("Cep");
       $Street = request("Street");
       $Number = request("Number");
       $Neighborhood = request("Neighborhood");
       $City = request("City");
       $State = request("State");
       $Complement = request("Complement");

       $Printname = request("Printname");      
       $Creditnumber = request("Creditnumber");
       $Securecode = request("Securecode");
       $Datevalidate = request("Datevalidate");
       $Printname = Crypt::encrypt($Printname);
       $Creditnumber = Crypt::encrypt($Creditnumber);
       $Securecode = Crypt::encrypt($Securecode);
       $Datevalidate = Crypt::encrypt($Datevalidate);

        if ($idAssured != null){

            $objAssured = AssuredModel::FindAssured($idAssured);
            $objAssured->Name =  $Name;
            $objAssured->Cpf = $Cpf; 
            $objAssured->PhoneFix = $PhoneFix;
            $objAssured->PhoneCel = $PhoneCel;
            $objAssured->Cep = $Cep; 
            $objAssured->Street = $Street;
            $objAssured->Number = $Number;
            $objAssured->Neighborhood = $Neighborhood;
            $objAssured->City = $City;
            $objAssured->State = $State;
            $objAssured->Complement = $Complement;
            $objAssured->Printname = $Printname;
            $objAssured->Creditnumber = $Creditnumber;
            $objAssured->Securecode = $Securecode;
            $objAssured->Datevalidate = $Datevalidate;
            $objAssured->save();

        }else{

            $objAssured =  new AssuredModel();
            $objAssured->userid =  session("iduser");
            $objAssured->Name =  $Name;
            $objAssured->Cpf = $Cpf; 
            $objAssured->PhoneFix = $PhoneFix;
            $objAssured->PhoneCel = $PhoneCel;
            $objAssured->Cep = $Cep; 
            $objAssured->Street = $Street;
            $objAssured->Number = $Number;
            $objAssured->Neighborhood = $Neighborhood;
            $objAssured->City = $City;
            $objAssured->State = $State;
            $objAssured->Complement = $Complement;
            $objAssured->Printname = $Printname;
            $objAssured->Creditnumber = $Creditnumber;
            $objAssured->Securecode = $Securecode;
            $objAssured->Datevalidate = $Datevalidate; 
            $objAssured->save();
            
        }
        
        return redirect()->route('home');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EstateModel;
use App\CategoryEstateModel;
use Illuminate\Support\Facades\Auth;
class EstateController extends Controller
{
    public function index(){        
        $objEstate = EstateModel::FindEstateUser()->get();
        return view('Estate.index', [
            'objEstate' => $objEstate
        ]);
    }

    public function create(){        
        $objCategoryEstate = CategoryEstateModel::all();
        return view('Estate.create', [
            'objCategoryEstate' => $objCategoryEstate
        ]);
    }

    public function edit($id){
        $objEstateEdit = EstateModel::FindEstatebyId($id)->first();
        return view('Estate.edit', [
            'objEstateEdit' => $objEstateEdit
        ]);
    }

    public function save(){

       $iduser = request("iduser");
       $idEstate = request("idEstate");
       $Name = request("Name");
       $UrlPhoto = request("UrlPhoto");
       $idCategoryEstate = request("SelectedCategory");
       $Modelo = request("Modelo");
       $Description = request("Description");
       $Placa = request("Placa");
       $Renavam = request("Renavam");
       $Chassi = request("Chassi");
       $Color = request("Color");
       $Year = request("Year");
       $Serial = request("Serial");
       $Imei = request("Imei");
       $State = request("State");
       $City = request("City");
       $Street = request("Street");
       $Neighborhood = request("Neighborhood");
       $Number = request("Number");
       $Cep = request("Cep");
       $Complement = request("Complement");
       $Dimensions = request("Dimensions");
       $userid = Auth::id();

       

        if ($idEstate != null){

           

            $objEstate = EstateModel::findOrFail($idEstate);

           
            $objEstate->Name = $Name;
            $objEstate->UrlPhoto = $UrlPhoto;
            $objEstate->idCategoryEstate = $idCategoryEstate;
            $objEstate->Modelo = $Modelo;
            $objEstate->Description = $Description;
            $objEstate->Placa = $Placa;
            $objEstate->Renavam = $Renavam;
            $objEstate->Chassi = $Chassi;
            $objEstate->Color = $Color;
            $objEstate->Year = $Year;
            $objEstate->Serial = $Serial;
            $objEstate->Imei = $Imei;
            $objEstate->State = $State;
            $objEstate->City = $City;
            $objEstate->Street = $Street;
            $objEstate->Neighborhood = $Neighborhood;
            $objEstate->Number = $Number;
            $objEstate->Cep = $Cep;
            $objEstate->Complement = $Complement;
            $objEstate->Dimensions = $Dimensions;
            $objEstate->save();

        }else{

            $objEstate = new EstateModel();
            $objEstate->iduser =  $userid;
            $objEstate->Name =  $Name;          
            $objEstate->UrlPhoto = $UrlPhoto;
            $objEstate->idCategoryEstate = $idCategoryEstate;
            $objEstate->Modelo = $Modelo;
            $objEstate->Description = $Description;
            $objEstate->Placa = $Placa;
            $objEstate->Renavam = $Renavam;
            $objEstate->Chassi = $Chassi;
            $objEstate->Color = $Color;
            $objEstate->Year = $Year;
            $objEstate->Serial = $Serial;
            $objEstate->Imei = $Imei;
            $objEstate->State = $State;
            $objEstate->City = $City;
            $objEstate->Street = $Street;
            $objEstate->Neighborhood = $Neighborhood;
            $objEstate->Number = $Number;
            $objEstate->Cep = $Cep;
            $objEstate->Complement = $Complement;
            $objEstate->Dimensions = $Dimensions;
            $objEstate->save();
            
        }

            return redirect('estate');

    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\AssuredModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $idAssured = 0;

            $userid = Auth::id();
            $objAssured = AssuredModel::where('userid',$userid)->get();
            foreach ($objAssured as $objAssure) {
                $idAssured = $objAssure->idAssured;
            }
            session(['idAssured' => $idAssured]);
            session(['iduser' => $userid]);
            
      

        return view('home');
    }
}

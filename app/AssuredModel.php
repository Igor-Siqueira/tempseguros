<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssuredModel extends Model
{
    protected $fillable = [
        'idAssured',
        'userid',
        'Name',
        'Cpf',
        'Cep',
        'State',
        'City',
        'Street',
        'Neighborhood',
        'Number',
        'Complement',
        'PhoneFix',
        'PhoneCel',
        'Printname',
        'Creditnumber',
        'Securecode',
        'Datevalidate',
        'created_at',
        'updated_at'
    ]; 

    protected $table = 'Assured';
    protected $primaryKey = 'idAssured';

    public static function FindAssured($idAssured)
    {
        return static::where('idAssured', $idAssured)->first();
    }
}
